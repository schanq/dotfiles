let g:python3_host_prog = '/usr/local/var/pyenv/versions/py3nvim/bin/python'
set runtimepath ^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc
