" Polyglot
let g:polyglot_disabled = ['markdown']

call plug#begin()
Plug 'rust-lang/rust.vim'
Plug 'FooSoft/vim-argwrap'
Plug 'ervandew/supertab'
Plug 'flazz/vim-colorschemes'
Plug 'godlygeek/tabular'
Plug 'nvie/vim-flake8'
Plug 'radenling/vim-dispatch-neovim'
Plug 'preservim/nerdtree'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'nvim-lua/plenary.nvim'
Plug 'sindrets/diffview.nvim'
Plug 'numirias/semshi', { 'do': ':UpdateRemotePlugins' }
Plug 'vim-airline/vim-airline'
Plug 'xolox/vim-misc'
Plug 'alok/notational-fzf-vim'
Plug 'dense-analysis/ale'
Plug 'arcticicestudio/nord-vim'
Plug 'vimwiki/vimwiki'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'michal-h21/vim-zettel'
Plug 'yuezk/vim-js'
call plug#end()

" NERDTREE SETUP
" ==============
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if(winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q| endif
let g:NERDTreeShowLineNumbers=1
autocmd BufEnter NERD_* setlocal rnu
let NERDTreeIgnore = ['\.pyc$', '__pycache__']

" EDITING SETUP
" =============
set tw=100
set hlsearch
set ruler
set tabstop=4
set shiftwidth=4
set expandtab
set backspace=indent,eol,start
set background=dark
set termguicolors
set autoindent
set mouse=a
set incsearch
set ignorecase
set smartcase
set hlsearch
set wildmode=longest,list ",full
set wildmenu
set shell=/usr/local/bin/zsh
autocmd FileType make setlocal noexpandtab
let &t_SI = "\<Esc>]50;CursorShape=1\x7"
let &t_EI = "\<Esc>]50;CursorShape=0\x7"
set guicursor=
let g:pymode_rope = 0
let g:pymode_folding=0
let g:terminal_color_4 = '#ff0000'
let g:terminal_color_5 = 'green'
tnoremap <C-w>h <C-\><C-n><C-w>h
tnoremap <C-w>j <C-\><C-n><C-w>j
tnoremap <C-w>k <C-\><C-n><C-w>k
tnoremap <C-w>l <C-\><C-n><C-w>l
au BufEnter * if &buftype == 'terminal' | :startinsert | endif
nnoremap ,b :buffer *
set wildmenu
set wildignore+=*.swp,*.bak
set wildignore+=*.pyc,*.class,*.sln,*.Master,*.csproj,*.csproj.user,*.cache,*.dll,*.pdb,*.min.*
set wildignore+=*/.git/**/*,*/.hg/**/*,*/.svn/**/*
set wildignore+=*/min/*,*/vendor/*,*/node_modules/*,*/bower_components/*
set wildignore+=tags,cscope.*
set wildignore+=*.tar.*
set wildignorecase
set wildmode=full

" Colorscheme
autocmd ColorScheme * highlight Normal ctermbg=NONE guibg=NONE
colorscheme fahrenheit

" Custom Commands
" ===============
command -nargs=* Test split | terminal pytest <args>
command -nargs=* Py split | terminal ipython <args>
command -nargs=* Git split | terminal git <args>
command -nargs=* Node split | terminal node <args>

" ArgWrap setup
nnoremap <silent> <leader>a :ArgWrap<CR>
let g:argwrap_wrap_closing_brace = 0

" Terminal
tnoremap <Esc> <C-\><C-n>

" Python
let python_highlight_all=1
autocmd BufWritePre * :%s/\s\+$//e
function SemshiOverrides()
    hi semshiLocal           ctermfg=209 guifg=#ffd787
    hi semshiGlobal          ctermfg=214 guifg=#ffb625
    hi semshiImported        ctermfg=214 guifg=#ff9911 cterm=bold gui=bold
    hi semshiParameter       ctermfg=75  guifg=#ffebc3
    hi semshiParameterUnused ctermfg=117 guifg=#af5f5f cterm=underline gui=underline
    hi semshiFree            ctermfg=218 guifg=#87a5c3
    hi semshiBuiltin         ctermfg=207 guifg=#ff9621
    hi semshiAttribute       ctermfg=49  guifg=#ffffd7
    hi semshiSelf            ctermfg=249 guifg=#afc3d6
    hi semshiUnresolved      ctermfg=226 guifg=#af5f5f cterm=underline gui=underline
    hi semshiSelected        ctermfg=231 guifg=#ffffff ctermbg=161 guibg=#2b5d23

    hi semshiErrorSign       ctermfg=231 guifg=#ffffff ctermbg=160 guibg=#d70000
    hi semshiErrorChar       ctermfg=231 guifg=#ffffff ctermbg=160 guibg=#d70000
    sign define semshiError text=E> texthl=semshiErrorSign
endfunction
let g:semshi#excluded_hl_groups	= [ 'local', 'attribute', 'free', 'global', 'parameter', 'self', 'imported' ]
autocmd FileType python call SemshiOverrides()
autocmd ColorScheme * call SemshiOverrides()

" Spellcheck
autocmd FileType markdown setlocal spell
autocmd FileType gitcommit setlocal spell

" Vim Wiki
filetype plugin on
let zettel_path = '~/Code/personal/zettelkasten/'
let g:vimwiki_list = [{'path': zettel_path,'ext':'.md','syntax':'markdown'}]
let g:zettel_format = "%y%m%d-%H%M-%title"
let g:zettel_options = [{"front_matter" : {"tags" : ""}, "template" : "~/Code/personal/zettelkasten/zettel_template.tpl"}]
let g:nv_search_paths = [zettel_path]
let g:nv_use_short_pathnames=1
autocmd BufNewFile */diary/[0-9]*.md :silent 0r !echo '----------\n:personal:\n\n--------------\n:professional:\n\n'
nnoremap Ω :ZettelNew

" Fzf
command -nargs=* Anki tabnew | terminal python3 "/Users/pierre/Code/personal/anki-github/anki.py" "/Users/pierre/Code/personal/zettelkasten" <args>
command! -bang -nargs=* ZettelQuiz call fzf#vim#ag('\*\*Q\*\*:', fzf#vim#with_preview({'dir': zettel_path}, 'up:40%:hidden', 'ctrl-/'), <bang>0)
nmap œ :Anki
nmap ƒ :Files<CR>
nmap ß :Ag<CR>

" YouCompleteMe
let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"
let g:ycm_python_binary_path = 'python'

"Pretty format XML
com! FormatXML :%!python3 -c "import xml.dom.minidom, sys; print(xml.dom.minidom.parse(sys.stdin).toprettyxml())"
nnoremap = :FormatXML<Cr>

" Diffs
nnoremap ∂ :DiffviewOpen

" FILETYPES
" =========
au BufNewFile,BufRead *.rs set filetype=rust
au BufNewFile,BufRead *.toml set filetype=toml

" STARTUP ACTIONS
" ===============
cd .
set path=$PWD/**
set complete-=i
