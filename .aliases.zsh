# PRIVATE VARS
if [ -f ~/.private-local-config.zsh ]
then
    source ~/.private-local-config.zsh
fi

# ALIASES
# =======
alias v="nvim ~/Code/personal/zettelkasten/index.md -p -c ':tabNext'"
alias vi=v
alias vim=vi
alias vd="nvim -d"
alias ls=lsd
alias zconf="v ~/.zshrc"
alias vconf="v ~/.vimrc"
alias showhidden="defaults write com.apple.finder AppleShowAllFiles YES;killall Finder;"
alias hidehidden="defaults write com.apple.finder AppleShowAllFiles NO;killall Finder;"
alias latest_commit_hash='git log | head -n1 | awk "{print \$2;}"'

docker-rm-all() {
    docker rm $(docker ps -a -q)
    docker rmi $(docker images -q)
    docker system prune --volumes
}

to-mp3 (){
    ffmpeg -i $1 -vn -ar 44100 -ac 2 -b:a 320k $2
}

to-mp4 (){
    ffmpeg -i $1 -vcodec h264 -acodec aac -strict -2 $2
}

to-wav (){
    ffmpeg -i $1 -acodec pcm_s16le -ac 2 -ar 16000 $2
}

clean-git-branches () {
    merged_branches=`git branch --merged | grep -v "\* master"`
    for branch in $merged_branches
    do
        git branch -d "$branch"
    done
    git remote prune origin
}
