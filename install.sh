sudo -v

# Install xcode command line tools
xcode-select --install
sudo xcodebuild -license accept

# Install brew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
brew update

# Install from brewfile
brew tap homebrew/bundle
brew bundle

# Make zsh the default shell
chsh -s $(which zsh)

# Install oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
cp .zshrc ~/.zshrc
zsh

# Setup Python
pyenv install 3.10-dev

# Install vim-plug and setup vim
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
mkdir ~/.config
cp init.vim ~/.config
cp .vimrc ~/.vimrc
vim +PlugInstall +qall

# Install apps from app store
function app_store_install() {
    mas search $1 | grep $1 | awk '{ print $1 }' | xargs mas install
}

app_store_install Irvue
app_store_install Jiffy
app_store_install Clocker
