# Dotfiles

My bootstrapped setup for new machines.

### Steps
1. Download Xcode
1. Run `sudo xcode-select -s /Applications/Xcode.app/Contents/Developer`
3. Run `sudo xcodebuild -license accept`
1. Run `git clone` into `~/.dotfiles`.
2. Run `cd ~/.dotfiles`.
3. Season `install.sh`, `Brewfile`, `.zshrc` and `.vimrc` to taste.
4. Run `. ./install.sh`
